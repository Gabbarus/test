#Подготовка

###Создание docker контейнеров

Копируем репозиторий в любую папку. Запускаем следующие команды

`# docker-compose build` для сборки docker образов

`# docker-compose up -d` для запуска контейнеров в фоновом режиме

###Создание базы данных

Вам надо подключиться к php-fpm контейнеру.

`# docker-compose exec php-fpm /bin/bash`

В командной строке контейнера выполняем

`# composer install` - установка всех необходимых пакетов

`# php bin/console doctrine:migrations:migrate` - выполняем миграции в базе данных

###Запуск UNIT тестов

Внутри контейнера php-fpm нужно запустить

`# php vendor/bin/codecept run unit`

###Поисковый запрос
http://localhost:8080/book/search?title=Название

