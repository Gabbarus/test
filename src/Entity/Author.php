<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 * @ORM\Table(name="author",indexes={
 *     @ORM\Index(name="author_idx", columns={"id"})
 * })
 */
class Author implements TimestampableInterface, TranslatableInterface
{
    use TimestampableTrait;
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="authors")
     */
    private Collection $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->addAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            $book->removeAuthor($this);
        }

        return $this;
    }

    public function getLocale(): string
    {
        return $this->getCurrentLocale();
    }

    public function setLocale(string $locale): self
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    public function getName(): ?string
    {
        /** @var AuthorTranslation $translation */
        $translation = $this->translate(
            null,
            false
        );

        return $translation->getName();
    }

    public function setName(string $name): self
    {
        /** @var AuthorTranslation $translation */
        $translation = $this->translate(
            null,
            false
        );

        $translation->setName($name);

        return $this;
    }
}
