<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @ORM\Table(name="book",indexes={
 *     @ORM\Index(name="book_idx", columns={"id"})
 * })
 */
class Book implements TimestampableInterface, TranslatableInterface
{
    use TimestampableTrait;
    use TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="books", cascade={"persist"})
     */
    private Collection $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): array
    {
        return $this->authors->toArray();
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->authors->removeElement($author);

        return $this;
    }

    public function getLocale(): string
    {
        return $this->getCurrentLocale();
    }

    public function setLocale(string $locale): self
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    public function getName(): ?string
    {
        /** @var BookTranslation $translation */
        $translation = $this->translate(
            null,
            false
        );

        return $translation->getName();
    }

    public function setName(string $name): self
    {
        /** @var BookTranslation $translation */
        $translation = $this->translate(
            null,
            false
        );

        $translation->setName($name);

        return $this;
    }


}
