<?php declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    /** {@inheritdoc}*/
    protected $code = Response::HTTP_BAD_REQUEST;

    /** @var ConstraintViolationListInterface */
    protected $errors;

    public function __construct(ConstraintViolationListInterface $errors, $message = 'Validation Failed')
    {
        parent::__construct($message, Response::HTTP_BAD_REQUEST);
        $this->errors = $errors;
    }

    public static function createFromCodeAndMessage(string $code, string $message): self
    {
        return new static(new ConstraintViolationList([
            new ConstraintViolation(
                $message,
                null,
                [],
                null,
                '',
                null,
                null,
                $code
            )
        ]), $message);
    }

    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }
}
