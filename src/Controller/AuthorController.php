<?php

namespace App\Controller;

use App\Handler\CreateAuthorHandler;
use App\Dto\Request\CreateAuthorDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    private CreateAuthorHandler $createHandler;

    public function __construct(CreateAuthorHandler $createHandler)
    {
        $this->createHandler = $createHandler;
    }

    /**
     * @Route("/author/create", methods={"POST"})
     */
    public function create(CreateAuthorDto $dto): Response
    {
        $result = $this->createHandler->handle($dto);
        if (!$result->isSuccess()) {
            return new JsonResponse(['code' => 400, 'message' => $result->getErrorMessage()], 400);
        }

        return new JsonResponse(['id' => $result->getId()]);
    }
}
