<?php

namespace App\Controller;

use App\Handler\CreateBookHandler;
use App\Dto\Request\CreateBookDto;
use App\Dto\Transformer\Response\BookListTransformer;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    private CreateBookHandler $createHandler;
    private BookRepository $repository;
    private BookListTransformer $transformer;

    public function __construct(
        CreateBookHandler $createHandler,
        BookRepository $repository,
        BookListTransformer $transformer
    )
    {
        $this->createHandler = $createHandler;
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    /**
     * @Route("/book/create", methods={"POST"})
     */
    public function create(CreateBookDto $dto): Response
    {
        $result = $this->createHandler->handle($dto);

        if (!$result->isSuccess()) {
            return new JsonResponse(['code' => 400, 'message' => $result->getErrorMessage()], 400);
        }

        return new JsonResponse(['id' => $result->getId()]);
    }

    /**
     * @Route("/book/search", methods={"GET"})
     */
    public function search(Request $request): Response
    {
        if (null === $title = $request->get('title', null)) {
            $message = 'Query parameter "title" is required';

            return new JsonResponse(['code' => 400, 'message' => $message], 400);
        }
        $books = $this->repository->findByTitle($title);

        return new JsonResponse(['collection' => $this->transformer->transformList($books)]);
    }

    /**
     * @Route(
     *     "/{_locale}/book/{id}",
     *     requirements={
     *         "_locale": "en|ru",
     *     }
     * )
     */
    public function detail(int $id, Request $request): Response
    {
        if (null === $book = $this->repository->find($id)) {
            $message = sprintf('Book with id %d not found', $id);

            return new JsonResponse(['code' => 400, 'message' => $message], 400);
        }
        $this->transformer->setLocale($request->getLocale());

        return new JsonResponse($this->transformer->transform($book));
    }
}
