<?php declare(strict_types=1);

namespace App\ParamConverter;

use App\Dto\Request\CreateAuthorDto;
use App\Dto\Request\CreateBookDto;
use App\Dto\Request\LocaleValueDto;
use App\Dto\Request\SerializedRequest;
use App\Exception\ValidationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SerializerParamConverter implements ParamConverterInterface
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        try {
            $model = $this->serializer->deserialize($request->getContent(), $configuration->getClass(), 'json');
        } catch (\Throwable $e) {
            throw ValidationException::createFromCodeAndMessage(
                'REQUEST_CONTENT_DESERIALIZATION_ERROR',
                $e->getMessage(),
            );
        }

        $errors = $this->validator->validate($model);

        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $request->attributes->set($configuration->getName(), $model);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        if ($configuration->getClass() === null) {
            return false;
        }

        if (array_key_exists(SerializedRequest::class, class_implements($configuration->getClass()))) {
            return true;
        }

        $supportedClasses = [
            LocaleValueDto::class,
            CreateAuthorDto::class,
            CreateBookDto::class,
        ];

        return in_array($configuration->getClass(), $supportedClasses);
    }
}
