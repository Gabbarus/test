<?php declare(strict_types=1);

namespace App\Dto\Transformer\Response;

use App\Dto\Request\LocaleValueDto;
use App\Dto\Response\AuthorResponseDto;
use App\Dto\Response\BookResponseDto;
use App\Entity\Author;
use App\Entity\Book;


class AuthorTransformer
{
    private ?string $currentLocale;

    public function __construct(?string $locale = null)
    {
        $this->currentLocale = $locale;
    }

    public function transform(Author $author): AuthorResponseDto
    {
        $dto = new AuthorResponseDto();
        $dto->id = $author->getId();
        if ($this->currentLocale){
            $dto->name = $author->setLocale($this->currentLocale)->getName();
        }
        else {
            $names = [];
            foreach (LocaleValueDto::getAllowedLocales() as $locale){
                $names[] = $author->setLocale($locale)->getName();
            }
            $dto->name = implode('|', $names);
        }

        return $dto;
    }

    public function transformList(array $items): array
    {
        return array_map([$this, 'transform'], $items);
    }

    public function setLocale(string $locale): self
    {
        $this->currentLocale = $locale;

        return $this;
    }

    public function getCurrentLocale(): string
    {
        return $this->currentLocale;
    }
}
