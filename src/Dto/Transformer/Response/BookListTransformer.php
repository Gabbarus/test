<?php declare(strict_types=1);

namespace App\Dto\Transformer\Response;

use App\Dto\Request\LocaleValueDto;
use App\Dto\Response\BookResponseDto;
use App\Entity\Book;

class BookListTransformer
{
    private ?string $currentLocale = null;

    public function transform(Book $book): BookResponseDto
    {
        $dto = new BookResponseDto();

        $dto->id = $book->getId();
        $authorTransformer = new AuthorTransformer();
        if ($this->currentLocale){
            $dto->name = $book->setLocale($this->currentLocale)->getName();
            $authorTransformer->setLocale($this->currentLocale);
        }
        else {
            $names = [];
            foreach (LocaleValueDto::getAllowedLocales() as $locale){
                $names[] = $book->setLocale($locale)->getName();
            }
            $dto->name = implode('|', $names);
        }
        $authors = $book->getAuthors();
        $dto->authors = $authorTransformer->transformList($authors);

        return $dto;
    }

    public function transformList(array $items): array
    {
        return array_map([$this, 'transform'], $items);
    }

    public function setLocale(string $locale): self
    {
        $this->currentLocale = $locale;

        return $this;
    }

    public function getCurrentLocale(): ?string
    {
        return $this->currentLocale;
    }
}
