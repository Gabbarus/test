<?php declare(strict_types=1);

namespace App\Dto\Request;;

use Symfony\Component\Validator\Constraints as Assert;

class SearchBookRequest
{

    /**
     * @Assert\Length(
     *     min=3,
     *     payload={"code": "STRING_TOO_SHORT"})
     *     )
     */
    private ?string $searchQuery = null;

    public function __construct()
    {

    }

    public function getSearchQuery(): ?string
    {
        return $this->searchQuery;
    }

    public function setSearchQuery(string $searchQuery): self
    {
        $this->searchQuery = $searchQuery;

        return $this;
    }

}
