<?php declare(strict_types=1);

namespace App\Dto\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CreateAuthorDto
{
    /**
     * @Assert\NotBlank(payload={"code": "EMPTY_VALUE"})
     */
    /** @var LocaleValueDto[] */
    public array $name;
}
