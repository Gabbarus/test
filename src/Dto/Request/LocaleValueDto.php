<?php declare(strict_types=1);

namespace App\Dto\Request;

class LocaleValueDto
{
    private string $locale;
    private string $value;

    public static function getAllowedLocales(): array
    {
        return ['en', 'ru' ];
    }

    public function __construct(string $locale, string $value)
    {
        $this->locale = $locale;
        $this->value = $value;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
