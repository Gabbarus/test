<?php declare(strict_types=1);

namespace App\Dto\Response;

class BookResponseDto
{
    public int $id;
    public string $name;
    public array $authors;
}
