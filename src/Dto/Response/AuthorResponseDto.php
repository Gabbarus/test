<?php declare(strict_types=1);

namespace App\Dto\Response;

class AuthorResponseDto
{
    public int $id;
    public string $name;
}
