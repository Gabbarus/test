<?php declare(strict_types=1);

namespace App\Dto\Response;

class CreateResponseDto
{

    private bool $success;
    private ?int $id;
    private ?string $errorMessage;

    public function __construct(bool $success, ?int $id, ?string $errorMessage = null)
    {
        $this->success = $success;
        $this->id = $id;
        $this->errorMessage = $errorMessage;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public static function success(int $id): self
    {
        return new self(true, $id );
    }

    public static function failed(string $message): self
    {
        return new self(false, null, $message);
    }
}
