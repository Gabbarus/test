<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\AuthorTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function findOneByNameAndLocale(string $name, string $locale): ?Author
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin(AuthorTranslation::class, 'at', Join::WITH, 'a.id = at.translatable')
            ->andWhere('at.name = :name')
            ->andWhere('at.locale = :locale')
            ->setParameter('name', $name)
            ->setParameter('locale', $locale)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findByIds(array $ids): ?array
    {
        $qb = $this->createQueryBuilder('a');
        $qb->andWhere($qb->expr()->in('a.id', $ids));

        return $qb->getQuery()->getResult();
    }
}
