<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\BookTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findOneByNameAndLocale(string $name, string $locale): ?Book
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin(BookTranslation::class, 'at', Join::WITH, 'a.id = at.translatable')
            ->andWhere('at.name = :name')
            ->andWhere('at.locale = :locale')
            ->setParameter('name', $name)
            ->setParameter('locale', $locale)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findByTitle(string $title): ?array
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin(BookTranslation::class, 'at', Join::WITH, 'a.id = at.translatable')
            ->andWhere("at.name like '%" . $title . "%'");

        return $qb->getQuery()->getResult();
    }
}
