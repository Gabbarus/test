<?php declare(strict_types=1);

namespace App\Handler;

use App\Dto\Request\CreateAuthorDto;
use App\Dto\Request\LocaleValueDto;
use App\Dto\Response\CreateResponseDto;
use App\Entity\Author;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CreateAuthorHandler
{
    private AuthorRepository $repository;
    private EntityManagerInterface $entityManager;

    public function __construct(AuthorRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    public function handle(CreateAuthorDto $dto): CreateResponseDto
    {
        $requiredLocales = ['ru', 'en'];
        $translations = [];
        array_walk($dto->name, function (LocaleValueDto $item) use (&$translations) {
            $translations[$item->getLocale()] = $item->getValue();
        });

        foreach ($requiredLocales as $locale) {
            if (!array_key_exists($locale, $translations)) {
                $message = sprintf('Translation for field Name and required locale "%s" not found', $locale);

                return CreateResponseDto::failed($message);
            }
            if ($this->repository->findOneByNameAndLocale($translations[$locale], $locale)) {
                $message = sprintf('Author with name "%s" already exist', $translations[$locale]);

                return CreateResponseDto::failed($message);
            }
        }

        $author = new Author();
        foreach ($translations as $locale => $name) {
            $author->translate($locale)->setName($name);
        }
        $this->entityManager->persist($author);
        $author->mergeNewTranslations();
        $this->entityManager->flush();

        return CreateResponseDto::success($author->getId());
    }
}
