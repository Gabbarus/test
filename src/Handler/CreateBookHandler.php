<?php declare(strict_types=1);

namespace App\Handler;

use App\Dto\Request\CreateBookDto;
use App\Dto\Request\LocaleValueDto;
use App\Dto\Response\CreateResponseDto;
use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CreateBookHandler
{
    private BookRepository $repository;
    private EntityManagerInterface $entityManager;
    private AuthorRepository $authorRepository;

    public function __construct(
        BookRepository $repository,
        EntityManagerInterface $entityManager,
        AuthorRepository $authorRepository)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
        $this->authorRepository = $authorRepository;
    }

    public function handle(CreateBookDto $dto): CreateResponseDto
    {
        $requiredLocales = ['ru', 'en'];
        $translations = [];
        array_walk($dto->name, function (LocaleValueDto $item) use (&$translations) {
            $translations[$item->getLocale()] = $item->getValue();
        });

        foreach ($requiredLocales as $locale) {
            if (!array_key_exists($locale, $translations)) {
                $message = sprintf('Translation for field Name and required locale "%s" not found', $locale);

                return CreateResponseDto::failed($message);
            }
            if ($this->repository->findOneByNameAndLocale($translations[$locale], $locale)) {
                $message = sprintf('Book with title "%s" already exist', $translations[$locale]);

                return CreateResponseDto::failed($message);
            }
        }

        $authors = $this->authorRepository->findByIds(array_unique($dto->authorIds));

        $book = new Book();
        foreach ($translations as $locale => $name) {
            $book->setLocale($locale)->setName($name);
        }
        foreach ($authors as $author){
            $book->addAuthor($author);
        }
        $this->entityManager->persist($book);
        $book->mergeNewTranslations();
        $this->entityManager->flush();

        return CreateResponseDto::success($book->getId());
    }
}
