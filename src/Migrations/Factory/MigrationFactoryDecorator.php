<?php
declare(strict_types=1);

namespace App\Migrations\Factory;

use App\Migrations\Factory\Contract\FakerMigrationInterface;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Version\MigrationFactory;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationFactoryDecorator implements MigrationFactory
{
    private MigrationFactory $migrationFactory;
    private ContainerInterface $container;
    private Factory $fakerFactory;

    public function __construct(MigrationFactory $migrationFactory, ContainerInterface $container, Factory $fakerFactory)
    {
        $this->migrationFactory = $migrationFactory;
        $this->container = $container;
        $this->fakerFactory = $fakerFactory;
    }

    public function createVersion(string $migrationClassName): AbstractMigration
    {
        $instance = $this->migrationFactory->createVersion($migrationClassName);

        if ($instance instanceof ContainerAwareInterface) {
            $instance->setContainer($this->container);
        }

        if ($instance instanceof FakerMigrationInterface) {
            $instance->setFakerFactory($this->fakerFactory);
        }

        return $instance;
    }
}
