<?php

namespace App\Migrations\Factory\Contract;

use Faker\Factory;

interface FakerMigrationInterface
{
    public function setFakerFactory(Factory $factory);
}
