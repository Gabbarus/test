<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Migrations\Factory\Contract\FakerMigrationInterface;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Faker\Factory;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220103201840 extends AbstractMigration implements FakerMigrationInterface
{
    private array $authorIds = [];
    private Factory $fakerFactory;

    public function setFakerFactory(Factory $fakerFactory)
    {
        $this->fakerFactory = $fakerFactory;
    }

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        parent::preUp($schema);
        $query = "SELECT id FROM author";
        $data = $this->connection->executeQuery($query);
        foreach ($data as $row) {
            $this->authorIds[] = $row['id'];
        }
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX book_idx (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE book_author (book_id INT NOT NULL, author_id INT NOT NULL, INDEX IDX_9478D34516A2B381 (book_id), INDEX IDX_9478D345F675F31B (author_id), PRIMARY KEY(book_id, author_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE book_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_E69E0A132C2AC5D3 (translatable_id), INDEX name_idx (name), UNIQUE INDEX book_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_author ADD CONSTRAINT FK_9478D34516A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_author ADD CONSTRAINT FK_9478D345F675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_translation ADD CONSTRAINT FK_E69E0A132C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES book (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book_author DROP FOREIGN KEY FK_9478D34516A2B381');
        $this->addSql('ALTER TABLE book_translation DROP FOREIGN KEY FK_E69E0A132C2AC5D3');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE book_author');
        $this->addSql('DROP TABLE book_translation');
    }

    public function postUp(Schema $schema): void
    {
        parent::postUp($schema);
        $batchSize = 500;
        $ru_generator = $this->fakerFactory::create("ru_RU");
        $bookSQL = [];
        $bookAuthorsSQL = [];
        $bookTranslateSQL = [];
        for ($i = 1; $i <= 10000; $i++) {
            $createdAt = $ru_generator->dateTime->format('Y-m-d H:i:s');
            $bookSQL[] = "('" . $createdAt . "', '" . $createdAt . "')";

            $authorId = $ru_generator->randomElement($this->authorIds);
            $bookAuthorsSQL[] = "(" . $i . ", " . (int)$authorId . ")";

            $name_ru = $ru_generator->catchPhrase;
            $name_en = addslashes(transliterator_transliterate('Any-Latin;Latin-ASCII;', $name_ru));

            $bookTranslateSQL[] = "(" . $i . ", '" . $name_ru . "', 'ru'), (" . $i . ", '" . $name_en . "', 'en')";
            if (($i % $batchSize) === 0) {
                $SQL = 'INSERT INTO book (created_at, updated_at) VALUES ' . implode(', ', $bookSQL);
                $this->connection->executeQuery($SQL);
                $bookSQL = [];

                $SQL = 'INSERT INTO book_author (book_id, author_id) VALUES ' . implode(', ', $bookAuthorsSQL);
                $this->connection->executeQuery($SQL);
                $bookAuthorsSQL = [];

                $SQL = 'INSERT INTO book_translation (translatable_id, name, locale) VALUES ' . implode(', ',
                        $bookTranslateSQL);
                $this->connection->executeQuery($SQL);
                $bookTranslateSQL = [];
            }
        }
        if (count($bookSQL) > 0) {
            $SQL = 'INSERT INTO book (created_at, updated_at) VALUES ' . implode(', ', $bookSQL);
            $this->connection->executeQuery($SQL);

            $SQL = 'INSERT INTO book_author (book_id, author_id) VALUES ' . implode(', ', $bookAuthorsSQL);
            $this->connection->executeQuery($SQL);

            $SQL = 'INSERT INTO book_translation (translatable_id, name, locale) VALUES ' . implode(', ',
                    $bookTranslateSQL);
            $this->connection->executeQuery($SQL);
        }
    }
}
