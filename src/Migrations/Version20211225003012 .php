<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Migrations\Factory\Contract\FakerMigrationInterface;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Faker\Factory;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211225003012 extends AbstractMigration implements FakerMigrationInterface
{
    private Factory $fakerFactory;

    public function setFakerFactory(Factory $fakerFactory)
    {
        $this->fakerFactory = $fakerFactory;
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE author (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE author_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_E89826172C2AC5D3 (translatable_id), UNIQUE INDEX author_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE INDEX author_idx ON author (id)');
        $this->addSql('ALTER TABLE author_translation ADD CONSTRAINT FK_E89826172C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES author (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE author_translation DROP FOREIGN KEY FK_E89826172C2AC5D3');
        $this->addSql('DROP TABLE author');
        $this->addSql('DROP TABLE author_translation');
    }

    public function postUp(Schema $schema): void
    {
        parent::postUp($schema);
        $batchSize = 500;
        $ru_generator = $this->fakerFactory::create("ru_RU");
        $authorSQL = [];
        $authorTranslateSQL = [];
        for ($i = 1; $i <= 10000; $i++) {
            $createdAt = $ru_generator->dateTime->format('Y-m-d H:i:s');
            $authorSQL[] = "('" . $createdAt . "', '" . $createdAt . "')";

            $name_ru = $ru_generator->firstName . " " . $ru_generator->lastName;
            $name_en = transliterator_transliterate('Any-Latin;Latin-ASCII;', $name_ru);
            $name_en = addslashes($name_en);
            $authorTranslateSQL[] = "(" . $i . ", '" . $name_ru . "', 'ru'), (" . $i . ", '" . $name_en . "', 'en')";
            if (($i % $batchSize) === 0) {
                $SQL = 'INSERT INTO author (created_at, updated_at) VALUES ' . implode(', ', $authorSQL);
                $this->connection->executeQuery($SQL);
                $authorSQL = [];
                $SQL = 'INSERT INTO author_translation (translatable_id, name, locale) VALUES ' . implode(', ', $authorTranslateSQL);
                $this->connection->executeQuery($SQL);
                $authorTranslateSQL = [];
            }
        }
        if (count($authorSQL) > 0) {
            $SQL = 'INSERT INTO author (created_at, updated_at) VALUES ' . implode(', ', $authorSQL);
            $this->connection->executeQuery($SQL);
            $SQL = 'INSERT INTO author_translation (translatable_id, name, locale) VALUES ' . implode(', ', $authorTranslateSQL);
            $this->connection->executeQuery($SQL);
        }
    }
}
