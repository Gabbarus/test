<?php declare(strict_types=1);

namespace App\Tests\unit;

use App\Entity\Author;
use App\Tests\UnitTester;

class AuthorCest
{
    public function getName(UnitTester $I): void
    {
        $author = new Author();
        $author->setName('Leo Tolstoy');

        $I->assertSame('Leo Tolstoy', $author->getName());
    }

    public function getNameWithLocale(UnitTester $I): void
    {
        $author = new Author();
        $author->setLocale('ru')->setName('Лев Толстой');

        $I->assertSame('Лев Толстой', $author->setLocale('ru')->getName());
    }
}
