<?php declare(strict_types=1);

namespace App\Tests\unit;

use App\Handler\CreateAuthorHandler;
use App\Entity\Author;
use App\Entity\Book;
use App\Tests\UnitTester;

class BookCest
{

    private Author $testAuthor;

    protected function _before(UnitTester $I)
    {
        $testAuthor = new Author();
        $testAuthor->setName('Leo Tolstoy');
        $I->haveInRepository($testAuthor);
    }


    public function getName(UnitTester $I): void
    {
        $book = new Book();
        $book->setName('War & Peace');

        $I->assertSame('War & Peace', $book->getName());
    }

    public function getNameWithLocale(UnitTester $I): void
    {
        $book = new Book();
        $book->setLocale('ru')->setName('Война и Мир');

        $I->assertSame('Война и Мир', $book->setLocale('ru')->getName());
    }

    public function getAuthors(UnitTester $I): void
    {
        $book = new Book();
        $book->setName('War & Peace');
        $I->assertCount(0, $book->getAuthors());

        $testAuthor = new Author();
        $testAuthor->setName('Leo Tolstoy');
        $book->addAuthor($testAuthor);
        $authors= $book->getAuthors();
        $I->assertCount(1, $authors);
    }
}
